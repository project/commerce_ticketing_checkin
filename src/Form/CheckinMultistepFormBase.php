<?php

/**
 * @file
 * Contains \Drupal\demo\Form\MultistepFormBase.
 *
 * https://www.sitepoint.com/how-to-build-multi-step-forms-in-drupal-8/
 */

namespace Drupal\commerce_ticketing_checkin\Form;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class CheckinMultistepFormBase extends FormBase {

  /**
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  private $sessionManager;

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $currentUser;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * Constructs a \Drupal\demo\Form\Multistep\MultistepFormBase.
   *
   * @param \Drupal\user\PrivateTempStoreFactory $temp_store_factory
   * @param \Drupal\Core\Session\SessionManagerInterface $session_manager
   * @param \Drupal\Core\Session\AccountInterface $current_user
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, SessionManagerInterface $session_manager, AccountInterface $current_user) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->sessionManager = $session_manager;
    $this->currentUser = $current_user;

    $this->store = $this->tempStoreFactory->get('multistep_data');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.private_tempstore'),
      $container->get('session_manager'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Start a manual session for anonymous users.
    if ($this->currentUser->isAnonymous() && !isset($_SESSION['multistep_form_holds_session'])) {
      $_SESSION['multistep_form_holds_session'] = TRUE;
      $this->sessionManager->start();
    }

    $form = [];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
      '#weight' => 10,
    ];

    return $form;
  }

  public function formFieldKeys() {
    return ['user_input', 'ticket_quantity', 'tickets'];
  }

  /**
   * Saves the data from the multistep form.
   */
  protected function saveData() {
    // Logic for saving data goes here...

    \Drupal::messenger()->addMessage($this->t('Check in completed.'));
  }

  /**
   * Helper method that removes all the keys from the store collection used for
   * the multistep form.
   */
  protected function deleteStore(array $keys) {
    foreach ($keys as $key) {
      $this->store->delete($key);
    }
  }
  public function getTicketsForUser(array $variation_ids, $uid) {
    $database = \Drupal::database();
    $query = $database->select('commerce_ticket', 'ct');
    $query->fields('ct', ['id']);
    $query->join('commerce_order_item', 'coi', 'ct.order_item_id = coi.order_item_id');
    $query->join('commerce_product__variations', 'cpv', 'coi.purchased_entity = cpv.variations_target_id');
    $query->condition('ct.uid', $uid);
    $query->condition('cpv.variations_target_id', $variation_ids, 'IN');

    $ids = $query->execute()->fetchAllAssoc('id');

    return array_keys($ids);
  }

  public function searchTicketholders(array $variation_ids, $search_phrase) {
    $results = [];
    $database = \Drupal::database();
    $query = $database->select('commerce_ticket', 'ct');
    $query->fields('ct', ['uid']);
    $query->join('commerce_order_item', 'coi', 'ct.order_item_id = coi.order_item_id');
    $query->join('commerce_product__variations', 'cpv', 'coi.purchased_entity = cpv.variations_target_id');
    $query->condition('cpv.variations_target_id', $variation_ids, 'IN');
    $query->join('users_field_data', 'u', 'u.uid = ct.uid');

    $filter = $query->orConditionGroup()
      ->condition('u.name', '%' . $database->escapeLike($search_phrase) . '%', 'LIKE')
      ->condition('u.mail', '%' . $database->escapeLike($search_phrase) . '%', 'LIKE')
      ->condition('ct.ticket_number', '%' . $database->escapeLike($search_phrase) . '%', 'LIKE');
    $query->condition($filter);

    $uids = $query->execute()->fetchAllAssoc('uid');

    $counts = array_count_values(array_keys($uids));
    return $counts;
  }
}
