<?php

namespace Drupal\commerce_ticketing_checkin\Form;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\commerce_ticketing_checkin\Form\CheckinMultistepFormBase;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Plugin\views\filter\Access;

/**
 * Class ScannerForm.
 */
class CheckinSearchForm extends CheckinMultistepFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'checkin_search_form';
  }

  public function getTitle(Product $commerce_product) {
    return $this->t('Find ticketholder for @title', ['@title' => $commerce_product->label()]);
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, Product $commerce_product) {
    // Get query string values, and hash, and check against hash.
    $hash = \Drupal::request()->query->get('hash');

    $variations = \Drupal::request()->query->get('variations');
    $hashKey = $commerce_product->id() . $variations;
    $testHash = Crypt::hmacBase64($hashKey, \Drupal::config('system.site')->get('name'));
    if ($hash != $testHash) {
      return AccessResult::forbidden();
    }

    $variations = $commerce_product->getVariations();
    foreach ($variations as $variation) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationTypeInterface $variation_type */
      $variation_type = ProductVariationType::load($variation->bundle());
      if ($variation_type->hasTrait('purchasable_entity_ticket')) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Product $commerce_product = NULL) {
    $this->deleteStore($this->formFieldKeys());

    $form = parent::buildForm($form, $form_state);

    $form['user_input'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter your ticket number or e-mail address of the ticket purchaser'),
      '#required' => TRUE,
    ];

    $form_state->set('commerce_product', $commerce_product->id());

    $form['actions']['submit']['#value'] = $this->t('Search for ticket');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $variation_ids = explode(',', \Drupal::request()->query->get('variations'));
    $search_input = $form_state->getValue('user_input');
    $results = $this->searchTicketholders($variation_ids, $search_input);

    if (count($results) == 0) {
      $form_state->setErrorByName('user_input', $this->t('Cannot find any ticketholder using the information entered. Try an e-mail address or order number.'));
    } elseif (count($results) > 1) {
      $form_state->setErrorByName('user_input', $this->t('Cannot identify a specific ticketholder using the information entered. Try  more specific information, e.g. a full e-mail address or order number.'));
    } else {
      $this->store->set('tickets', $results);
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->store->set('user_input', $form_state->getValue('user_input'));
    $form_state->setRedirect(
      'commerce_ticketing_checkin.confirm_form',
      [
        'commerce_product' => $form_state->get('commerce_product'),
      ],
      [
        'query' => [
          'hash' => \Drupal::request()->query->get('hash'),
          'variations' => \Drupal::request()->query->get('variations'),
        ],
      ]
    );
  }
}
