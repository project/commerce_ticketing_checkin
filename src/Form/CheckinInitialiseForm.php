<?php

namespace Drupal\commerce_ticketing_checkin\Form;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Class CheckinInitialiseForm.
 */
class CheckinInitialiseForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'checkin_init_form';
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, Product $commerce_product) {
    $ticketed = FALSE;
    $variations = $commerce_product->getVariations();
    foreach ($variations as $variation) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationTypeInterface $variation_type */
      $variation_type = ProductVariationType::load($variation->bundle());
      if ($variation_type->hasTrait('purchasable_entity_ticket')) {
        $ticketed = TRUE;
        break;
      }
    }
    return AccessResult::allowedIf($ticketed && $account->hasPermission('inititate ticket check-in'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Product $commerce_product = NULL) {

    $form_state->set('commerce_product', $commerce_product->id());

    $options = [];
    $variations = $commerce_product->getVariations();
    foreach ($variations as $variation) {
      $labels = [];
      $labels[] = $variation->label();
      if ($variation->hasField('field_event_variation_title') && $variation->field_event_variation_title->value) {
        $labels[] = $variation->field_event_variation_title->value;
      }
      $options[$variation->id()] = implode(' - ', $labels);
    }
    $form['commerce_product_variation'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the product variations to allow check-ins for'),
      '#options' => $options,
      '#default_value' => array_keys($options),
    ];

    $form['logout'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log out for check-in form'),
      '#description' => $this->t('THIS IS STRONGLY RECOMMENDED. Otherwise whoever uses this device could do other things on this site with your user account.'),
      '#default_value' => TRUE,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Open check-in on this device'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('logout')) {
      user_logout();
    }

    $variations = implode(',', $form_state->getValue('commerce_product_variation'));
    $hashKey = $form_state->get('commerce_product') . $variations;
    $form_state->setRedirect(
      'commerce_ticketing_checkin.search_form',
      [
        'commerce_product' => $form_state->get('commerce_product'),
      ],
      [
        'query' => [
          'hash' => Crypt::hmacBase64($hashKey, \Drupal::config('system.site')->get('name')),
          'variations' => $variations,
        ],
      ]
    );
  }
}
